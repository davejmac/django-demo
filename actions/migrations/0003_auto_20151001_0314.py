# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('actions', '0002_auto_20150930_0341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='description',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
    ]
