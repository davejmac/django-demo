from django.conf.urls import patterns, url

from .views import ActionMonthView, ActionCreateView, ActionDetailView, ActionUpdateView, ActionDeleteView


urlpatterns = patterns('',
    url(r'^actions/current/$', ActionMonthView.as_view(), name='actions_current'),
    url(r'^action/create/$', ActionCreateView.as_view(), name='action_create'),
    url(r'^action/(?P<pk>[\w_-]+)/$', ActionDetailView.as_view(), name='action_detail'),
    url(r'^action/(?P<pk>[\w_-]+)/update/$', ActionUpdateView.as_view(), name='action_update'),
    url(r'^action/(?P<pk>[\w_-]+)/delete/$', ActionDeleteView.as_view(), name='action_delete'),
)
