from django.contrib import admin

from .models import Action


class ActionAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)

admin.site.register(Action, ActionAdmin)
