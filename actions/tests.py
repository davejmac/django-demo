from django.core.urlresolvers import reverse
from django.test import TestCase

from accounts.factories import UserFactory
from .factories import ActionFactory


class ActionMonthViewTest(TestCase):
    url_name = 'actions_current'

    def setUp(self):
        self.user = UserFactory.create()
        self.action_1 = ActionFactory.create(created_by=self.user)
        self.action_2 = ActionFactory.create(created_by=self.user)
        self.action_3 = ActionFactory.create(created_by=self.user)

    def test_get(self):
        self.client.login(email=self.user.email, password='password')
        response = self.client.get(reverse(self.url_name))
        self.assertEqual(200, response.status_code)
        self.assertTrue('results' in response.context)

    def test_get_not_logged_in(self):
        response = self.client.get(reverse(self.url_name))
        self.assertEqual(response.status_code, 302)


class ActionCreateViewTest(TestCase):
    url_name = 'action_create'

    def setUp(self):
        self.user = UserFactory.create()
        self.assigned_user = UserFactory.create()
        self.action_1 = ActionFactory.create(created_by=self.user)
        self.action_2 = ActionFactory.create(created_by=self.user)
        self.action_3 = ActionFactory.create(created_by=self.user)

    def test_get(self):
        self.client.login(email=self.user.email, password='password')
        response = self.client.get(reverse(self.url_name))
        self.assertEqual(200, response.status_code)

    def test_get_not_logged_in(self):
        response = self.client.get(reverse(self.url_name))
        self.assertEqual(response.status_code, 302)

    def test_post(self):
        self.client.login(email=self.user.email, password='password')
        data = {
            'title': 'Test Title',
            'description': 'Test Description',
            'assigned_to': self.assigned_user.pk,
            'created_by': self.user.pk
        }
        response = self.client.post(reverse(self.url_name), data=data)
        self.assertRedirects(response, reverse('actions_current'), status_code=302)

    def test_post_not_logged_in(self):
        data = {}
        response = self.client.post(reverse(self.url_name), data=data)
        self.assertEqual(302, response.status_code)
