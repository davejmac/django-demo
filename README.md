# demo

A repository demonstrating an ideal setup for a Django website including a basic application for user-created actions.

### Requirements

pip, npm

### Recommended

pyenv or virtualenv for easy Python version and dependency management

### Startup

1. ```pip install -r requirements.txt```

2. ```pip install -r requirements.dev.txt```

3. ```npm install```

4. ```./manage.py migrate```

5. ```./manage.py syncdb```

6. ```honcho start```

### Database

SQLite 3 is the database backend
