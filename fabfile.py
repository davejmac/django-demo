from fabric.api import sudo, env, lcd, cd, local, shell_env, get, settings
from fabric.decorators import runs_once, parallel

env.code_root = '/var/django/demo'


def secret_key():
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    key = get_random_string(50, chars)
    print key


def runserver():
    local('python manage.py runserver 0.0.0.0:8000')


def staging():
    env.hosts = []
    env.shell_env = {'STAGING': 'True'}


def production():
    env.hosts = []
    env.shell_env = {'PRODUCTION': 'True'}


@parallel
def pull():
    "Push out new code to the server."
    with cd(env.code_root):
        sudo('git pull')


@runs_once
def collectstatic():
    with shell_env(**env.shell_env):
        with cd(env.code_root):
            sudo('/var/virtualenvs/demo/bin/python manage.py collect_static --noinput --verbosity=2 --ignore="*.scss" --ignore="fonts*"', user='www-data')


@runs_once
def migrate():
    with shell_env(**env.shell_env):
        with cd(env.code_root):
            sudo('/var/virtualenvs/demo/bin/python manage.py migrate')


@parallel
def reload():
    """
    Reload gunicorn to pick up new code changes.
    """
    sudo('find %s -name "*.pyc" -exec rm -rf {} \;' % (env.code_root,))
    sudo("for pid in /var/django/*.pid; do echo $pid; sudo kill -HUP `cat $pid`; done")
    sudo('supervisorctl restart worker')


@parallel
def update_requirements():
    """
    Update pip requirements
    """
    with shell_env(**env.shell_env):
        with cd(env.code_root):
            sudo('/var/virtualenvs/demo/bin/pip install -r requirements.txt')


def deploy(skip_static=False):
    """
    skip_static allows skipping collect static files to speed up deployment of code only changes

    usage to skip static: fab <ENVIRONMENT> deploy:True
    usage to include collect static: fab <ENVIRONMENT> deploy
    """
    pull()
    update_requirements()
    if not skip_static:
        collectstatic()
    reload()


def local_timezone_setup():
    local('mysql_tzinfo_to_sql /usr/share/zoneinfo | sed -e "s/Local time zone must be set--see zic manual page/local/" | mysql -u root mysql')
