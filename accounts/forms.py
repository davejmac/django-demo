from django import forms

from .models import User


class RegisterForm(forms.ModelForm):
    """
    A form to register as a user on the site
    """

    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email address'}), error_messages={'required': 'Email address is required.'})
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}), error_messages={'required': 'Password is required.'})

    class Meta:
        model = User
        fields = ('email',)

    def clean_email(self):
        email = self.cleaned_data['email'].strip()
        if User.objects.filter(email__iexact=email).exists():
            raise forms.ValidationError('Email address already registered.')
        return email

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user
