from django.conf.urls import patterns, url

from .views import RegisterFormView


urlpatterns = patterns('django.contrib.auth.views',
    url(r'^login/$', 'login', name='login'),
    url(r'^logout/$', 'logout', {'next_page': '/'}, name='logout'),
    url(r'^password/reset/$', 'password_reset', name='password_reset'),
    url(r'^password/reset/done/$', 'password_reset_done', name='password_reset_done'),
    url(r'^password/reset/complete/$', 'password_reset_complete', name='password_reset_complete'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'password_reset_confirm', name='password_reset_confirm'),
    url(r'^password/change/$', 'password_change', name='change_password'),

    url(r'^register/$', RegisterFormView.as_view(), name='register'),
)
