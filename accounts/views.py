from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect

from .forms import RegisterForm


class RegisterFormView(CreateView):
    """
    Form view to allow users to register
    """

    form_class = RegisterForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('actions_current')

    def form_valid(self, form):
        self.object = form.save()
        account = authenticate(email=form.cleaned_data.get('email'), password=form.cleaned_data.get('password'))
        login(self.request, account)

        return HttpResponseRedirect(self.get_success_url())
