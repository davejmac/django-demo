import factory
from factory.django import DjangoModelFactory


class UserFactory(DjangoModelFactory):

    class Meta:
        model = 'accounts.User'

    first_name = factory.Sequence(lambda n: 'User%d' % n)
    last_name = 'LastName'
    email = factory.LazyAttribute(lambda obj: '%s@example.com' % obj.first_name)
    is_staff = False
    is_superuser = False

    @factory.post_generation
    def setuser_password(obj, create, extracted, **kwargs):
        obj.set_password('password')
        obj.save()


class AdminUserFactory(UserFactory):
    is_staff = True
