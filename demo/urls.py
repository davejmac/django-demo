from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = patterns('',
    url(r'^djangoadmin/', include(admin.site.urls)),

    url(r'', include('accounts.urls')),
    url(r'', include('actions.urls')),

    url(r'^$', TemplateView.as_view(template_name='index.html'), name='homepage'),
    url(r'^404/$', TemplateView.as_view(template_name='404.html'), name='404'),
    url(r'^500/$', TemplateView.as_view(template_name='500.html'), name='500'),
)
