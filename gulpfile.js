var gulp = require('gulp');
var sass = require('gulp-sass');
var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var plumber = require('gulp-plumber');
// var imagemin = require('gulp-imagemin');
// var jpegoptim = require('imagemin-jpegoptim');
// var jpegrecompress = require('imagemin-jpeg-recompress');
// var pngquant = require('imagemin-pngquant');
var notify = require('gulp-notify');
var through = require('through2');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat')
var _ = require('lodash');

gulp.task('default', ['sass', 'browserify', 'serve'], function() {
    console.log('Running Gulp');
});

// a list of bundles to build
var jsDir = './demo/static/js/';
var debug = false;

// Define Browserify targets with options
var browserifyBundles = [
    {
        entries: [ jsDir + 'main.js' ],
        cache: {},
        packageCache: {},
        fullPaths: false,
        outputName: 'main.js',
        debug: debug,
        require: []
    },
    {
        entries: [ jsDir + 'admin.js' ],
        cache: {},
        packageCache: {},
        fullPaths: false,
        outputName: 'admin.js',
        debug: debug,
        require: []
    }
];

gulp.task('browserify', function() {
    var browserifyThis = function(config) {
        // set some standard settings which are true for every bundle
        _.defaults(config, {
            paths: [jsDir],
            debug: debug,
            cache: {},
            packageCache: {},
            fullPaths: false,
        });

        // for all bundles which are not main.js, specify that everything
        // in main.js is external
        if(config.outputName !== 'main.js') {
            _.assign(config, {
                external: _.union(config.external, browserifyBundles[0].require)
            });
        }

        var b = browserify(config);

        var bundle = function() {
            reload();
            return b
                .bundle()
                .on('error', notify.onError({
                    title: 'Javascript Error',
                    message: function(err) {
                        process.stdout.write('\x07');
                        console.log(err);
                        var message = err.message;
                        var file = err.annotated.match(/\/([\w\d_-]+.js)/g)[1];
                        var line = err.line;
                        var column = err.column;
                        var error = message + ' in ' + file + ' at ' + line+':'+column;
                        return error;
                    },
                }))
                .pipe(source(config.outputName))
                .pipe(buffer())
                .pipe(uglify().on('error', gutil.log))
                .pipe(gulp.dest(jsDir + 'bundled/'));
        }

        if(config.external){ b.external(config.external) };
        if(config.require){ b.require(config.require) };

        b = watchify(b);
        b.on('update', bundle);

        bundle();
    };

    // get all bundles being browserified
    for(var i=0; i < browserifyBundles.length; i++) {
        browserifyThis(browserifyBundles[i]);
    }
});


// SASS
gulp.task('sass', function() {
    gulp.src('./demo/static/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: notify.onError({
                title: 'SASS Error',
                message: function(error) {
                    process.stdout.write('\x07');
                    console.log(error);
                    return error.message + ' on line '+error.lineNumber+' in '+error.fileName.split("/").slice(-1);
                }
            })
        }))
        .pipe(sass())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('./demo/static/css'));
    reload();
});


// Images
// @NOTE
// Needs further discussion/testing to settle on a set of options
// https://github.com/Mobelux/django-project-template/pull/6
//
// gulp.task('images', function() {
//     gulp.src('./demo/static/img/src/*')
//         .pipe(imagemin({
//             svgoPlugins: [{ removeViewBox: false }],
//             use: [
//                 jpegrecompress({ progressive: false, min: 90, target: 0.98, quality: 'high' }),
//                 pngquant()
//             ]
//         }))
//         .pipe(gulp.dest('./demo/static/img'));
// });


// Watch
gulp.task('serve', ['sass', 'browserify'], function() {
    browserSync.init({
        proxy: 'localhost:8000',
        ghostMode: true,
        open: false
    });
    gulp.watch(['./demo/static/img/src/*'], ['images']);
    gulp.watch(['./demo/static/sass/**/*.scss', './demo/static/sass/*.scss'], ['sass']);
    gulp.watch(['./demo/templates/**/*.html', './demo/templates/*.html', './demo/static/app_templates/*.html']).on('change', reload);
});
